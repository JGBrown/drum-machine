import React, { Component } from 'react';
import './DrumMachine.css';


class DrumMachine extends Component {

  displaybutton = (event) => {
    console.log("display event:",event)
    if (document.getElementById(event)){
      document.getElementById("display").innerHTML = `${event}`
 }
  };

  handleKeyDown = (event) => {
      console.log('event key ', event.key)
      if (document.getElementById(event.key.toUpperCase())){
        document.getElementById(event.key.toUpperCase()).play();
      }
      this.displaybutton(event.key.toUpperCase())

    }

    handleclick = (event) => {
        console.log('event key ', event.target.innerText)
        if (document.getElementById(event.target.innerText)){
          document.getElementById(event.target.innerText).play();
        }
        this.displaybutton(event.target.innerText)
      }

    componentDidMount(){
      document.addEventListener("keydown", this.handleKeyDown);
      document.addEventListener("click", this.handleclick)
  }

  render() {
    return <div id="drum-machine">
      <div id="display">some innerText</div>
      <div id="drumpadlayout">
      <div id="Heater1"  className="drum-pad" ><audio src="https://s3.amazonaws.com/freecodecamp/drums/Heater-1.mp3" className="clip" id="Q"></audio>Q</div>
      <div id="Heater2"  className="drum-pad"><audio src="https://s3.amazonaws.com/freecodecamp/drums/Heater-2.mp3" className="clip" id="W"></audio>W</div>
      <div id="Heater3"  className="drum-pad"><audio src="https://s3.amazonaws.com/freecodecamp/drums/Heater-3.mp3" className="clip" id="E"></audio>E</div>
      <div id="Heater4"  className="drum-pad"><audio src="https://s3.amazonaws.com/freecodecamp/drums/Heater-4_1.mp3" className="clip" id="A"></audio>A</div>
      <div id="Heater6"  className="drum-pad"><audio src="https://s3.amazonaws.com/freecodecamp/drums/Heater-6.mp3" className="clip" id="S"></audio>S</div>
      <div id="DscOh"  className="drum-pad"><audio src="https://s3.amazonaws.com/freecodecamp/drums/Dsc_Oh.mp3" className="clip"  id="D"></audio>D</div>
      <div id="KickNHat"  className="drum-pad"><audio src="https://s3.amazonaws.com/freecodecamp/drums/Kick_n_Hat.mp3"className="clip" id="Z"></audio>Z</div>
      <div id="RP4Kick"  className="drum-pad"><audio src="https://s3.amazonaws.com/freecodecamp/drums/RP4_KICK_1.mp3" className="clip" id="X"></audio>X</div>
      <div id="Cevh2"  className="drum-pad"><audio src="https://s3.amazonaws.com/freecodecamp/drums/Cev_H2.mp3" className="clip" id="C"></audio>C</div>
      </div>
    </div>;
  }
}
export default DrumMachine;
