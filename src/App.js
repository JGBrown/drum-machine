import React from 'react';
import './App.css';
import DrumMachine from './components/DrumMachine/DrumMachine';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        Drum Machine
          < DrumMachine />
      </header>
    </div>
  );
}

export default App;
